import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  user:any= {
    uid:''
  };
  token:string= '';

  constructor(
    public afAuth: AngularFireAuth,
    private router: Router
    ) {
    this.user = this.getUser();
    this.token = this.getToken();
  }

  login(social: string) {
    let loginsocial: any;
    switch (social) {
      case 'google': loginsocial = new auth.GoogleAuthProvider();
        break;
      case 'facebook': loginsocial = new auth.FacebookAuthProvider();
        break;
      default:
        break;
    }
    this.afAuth.auth.signInWithPopup(loginsocial).then(value =>{
      this.setUser(value.user);
      console.log(this.user);
      this.setToken(this.user.uid);
      this.router.navigate(['/home']);
    });
  }
  logout() {
    this.afAuth.auth.signOut();
    this.removeToken();
    this.removeUser();
  }

  getToken(): string {
    return localStorage.getItem('token');
  }

  setToken(token: string) {
    localStorage.setItem('token', token);
    this.token = this.getToken();
  }

  removeToken() {
    localStorage.removeItem('token');
    this.token = this.getToken();
  }

  getUser() {
    return JSON.parse(localStorage.getItem('client'));
  }
  setUser(obj: any) {
    localStorage.setItem('client', JSON.stringify(obj));
    this.user = this.getUser();
  }
  removeUser() {
    localStorage.removeItem('client');
    this.user = this.getUser();
  }
  loggedIn(): boolean {
    if (this.getToken()) {
      return true;
    } else {
      return false;
    }
  }
}
