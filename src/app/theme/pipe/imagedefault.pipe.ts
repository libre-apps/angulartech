import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'imagedefault'
})
export class ImagedefaultPipe implements PipeTransform {

  imgdefault: string = 'http://saveabandonedbabies.org/wp-content/uploads/2015/08/default.png';

  transform(image: any, args?: any): any {
    return image ? image : this.imgdefault;
  }

}
