import { Injectable } from '@angular/core';
import { ConfigService } from "../config/config.service";
import { environment } from "../../../environments/environment";
import { Observable } from 'rxjs';

// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/catch';
// import 'rxjs/add/operator/retry';
import { map, catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  private url = environment.jsonplace;
  constructor(private _http:ConfigService) { }

  getPosts(): Observable<any>{
    let ruta: string = `${this.url}/posts`;
    return this._http.get(ruta);
  }
  getPhotos(): Observable<any>{
    let ruta: string = `${this.url}/photos`;
    return this._http.get(ruta);
  }
}
