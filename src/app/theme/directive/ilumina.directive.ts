import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appIlumina]'
})
export class IluminaDirective {

  @Input() defaultColor: string;
  @Input('appIlumina') miColor: string;

  constructor(private elemento: ElementRef) {}

  @HostListener('mouseenter') onMouseEnter() {
    this.iluminame(this.miColor || this.defaultColor || 'red');
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.iluminame(null);
  }
  private iluminame(color: string) {
    this.elemento.nativeElement.style.backgroundColor = color;
  }
}
