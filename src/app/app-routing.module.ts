import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

// componentes
import { LoginComponent } from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { MapsComponent } from './pages/maps/maps.component';
import { GraficosComponent } from './pages/graficos/graficos.component';
import { ExampleCicloVidaComponent } from './pages/example-ciclo-vida/example-ciclo-vida.component';
import { FormulariosComponent } from './pages/formularios/formularios.component';

// Guards
import { LoginGuard } from './theme/guard/login.guard';
import { AuthGuard } from './theme/guard/auth.guard';

const routes: Routes = [
  {
    path: 'login', component: LoginComponent,
    canActivate: [LoginGuard]
  },
  { path: 'home', component: HomeComponent,
    canActivate: [AuthGuard]
  },
  { path: 'maps', component: MapsComponent,
    canActivate: [AuthGuard]
  },
  { path: 'graficos', component: GraficosComponent
  },
  { path: 'ciclovida', component: ExampleCicloVidaComponent
  },
  { path: 'formularios', component: FormulariosComponent
  },
  {
    path: '**', pathMatch: 'full', redirectTo: 'login'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
