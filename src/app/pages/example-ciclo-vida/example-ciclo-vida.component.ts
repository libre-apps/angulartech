import {
  Component,
  OnInit,
  OnChanges,
  DoCheck,
  AfterContentInit,
  AfterContentChecked,
  AfterViewInit,
  AfterViewChecked,
  OnDestroy
} from '@angular/core';

@Component({
  selector: 'app-example-ciclo-vida',
  templateUrl: './example-ciclo-vida.component.html',
  styleUrls: ['./example-ciclo-vida.component.css']
})
export class ExampleCicloVidaComponent implements OnChanges, OnInit,
  DoCheck, AfterContentInit, AfterContentChecked,
  AfterViewInit, AfterViewChecked, OnDestroy {

  color: string;

  constructor() {
    this.log('1.constructor');
  }

  ngOnChanges() {
    this.log('2.ngOnChanges');
  }

  ngOnInit() {
    this.log('3.ngOnInit');
  }

  ngDoCheck() {
    this.log('4.ngDoCheck');
  }

  ngAfterContentInit() {
    this.log('5.ngAfterContentInit');
  }

  ngAfterContentChecked() {
    this.log('6.ngAfterContentChecked');
  }

  ngAfterViewInit() {
    this.log('7.ngAfterViewInit');
  }

  ngAfterViewChecked() {
    this.log('8.ngAfterViewChecked');
  }

  ngOnDestroy() {
    this.log('9.ngOnDestroy');
  }

  private log(hook: string) {
    console.log(hook);
  }

}
