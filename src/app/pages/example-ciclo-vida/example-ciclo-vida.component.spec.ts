import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExampleCicloVidaComponent } from './example-ciclo-vida.component';

describe('ExampleCicloVidaComponent', () => {
  let component: ExampleCicloVidaComponent;
  let fixture: ComponentFixture<ExampleCicloVidaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExampleCicloVidaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExampleCicloVidaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
