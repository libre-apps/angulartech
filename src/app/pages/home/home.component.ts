import { Component, OnInit } from '@angular/core';
import { PostsService } from "../../theme/jsonplaceholder/posts.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  imagencargada = "https://e.radio-studio92.io/normal/2018/05/15/080108_610158.jpg";
  posts:any;
  photos: any=[];

  birthday = new Date(1988, 3, 15);
  format= 'shortDate';

  numero:number = 12.3;

  miimagen = null;

  // Youtube
  miYotube:string = "KeFoOn5BU2U";

  constructor(private _postsService:PostsService) {
    this.posts = [
    ];
    
  }


  ngOnInit() {
    this._postsService.getPosts()
      .subscribe(result =>{
        this.posts = result;
      });
    
      this._postsService.getPhotos()
      .subscribe(result =>{
        this.photos = result;
        console.log(this.photos);
      });

      setTimeout(() => {
        this.miimagen = this.imagencargada;
      }, 3000);
  }

  ClickFormat() {
    this.format= 'fullDate';
  }
}
