import { Component, OnInit } from '@angular/core';
import { LoginService } from "../../theme/services/login.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  // loginAut:AngularFireAuth;

  constructor(
    private _loginService:LoginService
    ) {
    // this.loginAut.user = null;
  }
  login(social:string) {
    this._loginService.login(social);
    // console.log(this._loginService.afAuth);
  }
  logout() {
    this._loginService.logout();
  }


  ngOnInit() {
  }

}
