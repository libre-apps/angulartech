import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.css']
})
export class MapsComponent implements OnInit {

  title: string = 'My first AGM project';
  lat: number = 51.678418;
  lng: number = 7.809007;

  marcadores:any =[];

  constructor() { }

  ngOnInit() {
  }

  clickMapa(evento){
    let nuevoMarcador ={
      lat: evento.coords.lat,
      lng: evento.coords.lng,
      draggable: true
    };
    this.marcadores.push(nuevoMarcador);
    console.log("marcador: ", this.marcadores);
  }
}
