import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ApplicationRef } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/****** Servicios ******/
// Config
import { ConfigService } from './theme/config/config.service';
import { LoginService } from './theme/services/login.service';
import { PostsService } from './theme/jsonplaceholder/posts.service';
// Guard
// Normales

/****** Modules ******/
import { AgmCoreModule } from '@agm/core';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { ChartsModule } from 'ng2-charts';

/****** Routes ******/
import { AppRoutingModule } from './app-routing.module';

/****** Enviroment ******/
import { environment } from '../environments/environment';

/****** Pipe ******/
import { ImagedefaultPipe } from './theme/pipe/imagedefault.pipe';
import { VideoYoutubePipe } from './theme/pipe/video-youtube.pipe';

/****** Directive ******/
import { IluminaDirective } from './theme/directive/ilumina.directive';

/****** Componentes ******/
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { HeaderComponent } from './reusable/header/header.component';
import { MapsComponent } from './pages/maps/maps.component';
import { ExampleCicloVidaComponent } from './pages/example-ciclo-vida/example-ciclo-vida.component';
import { GraficosComponent } from './pages/graficos/graficos.component';
import { FormulariosComponent } from './pages/formularios/formularios.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    HeaderComponent,
    MapsComponent,
    ExampleCicloVidaComponent,
    ImagedefaultPipe,
    VideoYoutubePipe,
    IluminaDirective,
    GraficosComponent,
    FormulariosComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AgmCoreModule.forRoot({
      apiKey: environment.mapsapikey
    }),
    ChartsModule
  ],
  providers: [
    ConfigService,
    LoginService,
    PostsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
