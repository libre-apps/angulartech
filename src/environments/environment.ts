// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  mapsapikey:"AIzaSyDzb0FB0Vd0_jUcN6Mj_HYHPmDfyMkhLvg",
  firebase: {
    apiKey: "AIzaSyDhGIypTzbf7pdjA0ag2E6csg5eIGRt-l4",
    authDomain: "claseangular-6e7f0.firebaseapp.com",
    databaseURL: "https://claseangular-6e7f0.firebaseio.com",
    projectId: "claseangular-6e7f0",
    storageBucket: "claseangular-6e7f0.appspot.com",
    messagingSenderId: "491779329989"
  },
  jsonplace: "https://jsonplaceholder.typicode.com"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
